本拉多·波裘是利維拉斯國內最優秀的賢者。

他對精靈有很深的了解，會操縱古代魔術式的一角，還有可能再現在遙遠時代裡的失落魔術。
在對魔術的熟知以及魔力量上，他立於利維拉斯國魔術師中的頂點。

其功績的源頭，是他壓倒性的求知慾。

雖然本拉多成長於宗教戰爭不斷的利維拉斯國之中，但他並未將利維伊認識為自己國家的神靈，而僅僅是規則性的引起魔法現象的精靈，再沒有在此之上的意味。

不說其他國家，對於尤其重視利維伊的言語的利維拉斯國來說，本拉多是很稀奇的魔術師。
在陷入激烈內亂的利維拉斯國裡，只有本拉多在帶領弟子運行研究設施，事不關己地持續研究。
由於他的研究內容過於越線而被其他派閥指為惡魔並襲擊，但他以自己的魔術製造出的合成魔獸十倍返還於敵人。

事實上，單單因為本拉多的反擊而被逼到衰退的宗派有不少。
然而縱然是絕頂的魔術師本拉多，也在研究中陷入停滯。

在解析魔法現象之前，必須要先解釋其解析結果。
理解越是深刻，視野越是寬廣，而未知的領域卻是一個勁地增加。

本拉多苦苦追求著理想中的魔術師形態——只要等價即實現一切的黃金之鍊金術師，但他在追求之中，知識越是深入，他就越是陷於龐大的魔術式裡，完全看不到彼岸。
於是他在這過程中，明白了兒時所抱負的理想是自己絕對無法到達的。

即使是如高精靈般長壽也無法達到。
本拉多究其一生所能完全理解構造的魔術式，僅僅只是深奧的魔術中相當淺顯的部分。

自己力所能及之道只是徒勞。

明白了這件事的時候，本拉多已經是垂垂老態。他臉上刻滿醜陋的皺紋，眼中蒙翳，手指顫抖，頭腦不復昔日的清明。
他耗費人生所開拓的魔術理論也消散在那個國家的戰亂之火中。
就算那些沒有消失，也不會有人能理解並繼承，幾代過後就會在這個世上消亡。

「我的研究，我的一生，都是無意義的嗎？」

在自己的研究設施的一個屋子裡，本拉多叩問自己。

『並非毫無意義。在如此短暫的人生中能探究魔術到如此程度，十分不錯。本拉多，汝可以與這個國家過去的名盛的魔術師們並列。這是由欺瞞了庫多爾、活過悠久的時代的、水之神利維伊的余所保證』

一道聲音回應了本拉多的自言自語。

「居然會被惡魔所干涉，我真是老了。好死不死，還是個叫做利維伊的存在。在這充滿妄執的利維拉斯裡，你這樣的惡魔毫不稀罕。以你這種傢伙為對手我早就習慣了，別做蠢事了。事到如今，下位的惡魔沒有什麼東西能教我的……而且我本來就不相信利維伊是真實存在」

『說余是下位惡魔麼，有趣』

伴隨著自稱利維伊的話語，本拉多的研究設施裡的魔力磁針發生偏轉。

（魔力干涉……！）

本拉多警戒著，握著長杖從座位上站起來。

『不必如此恐懼』

魔術式在本拉多的眼前交織。

不知道是何種魔術式，也不知道其意圖，但魔術式編織的速度很慢，也沒有保護術式的設定，將其抹消阻止發動很容易。
但是，這個為了發動魔術而展開的魔法陣未免太慢了。

覺察到有別的意圖的本拉多靜靜地看著魔術式在半空中編織。
編織完成的魔法陣上面，浮現出一個水球。

「還以為是什麼東西，原來僅僅是個兜圈子的水之魔術……？」

浮現出水球的魔法陣崩壞了。由於失去了魔法陣的效果，水球破碎消失了。
然而，崩壞的魔法陣發生變形，形成了三個完全不同的魔法陣。
三個魔法陣展現出規律性的現象，出現了三個水球。三個水球描繪著橢圓的軌道來回跳動。

「這，這是！？」

重新施展同一個魔術並不困難。
但是，將魔法陣變形，以其為基礎編織其他的魔法陣，需要有對魔法陣的廣泛且深刻的理解。
也就是說，這是以魔術的知識來挑釁，像在玩謎題遊戲一樣。

三個魔法陣再次破碎，生成的水球消失了。
不完全的魔法陣上魔術式再次構築，形成了六個魔法陣。
這次浮現的水球顏色不斷變化，發出光亮在本拉多周圍做螺旋運動。

「不，不可能……！儘管每個都是單純的魔術……但把魔法陣分割，多次使用魔術式……這種事情，這種戲法不將魔術式以最小單位理解是做不到的！這是四大創造神在展示其自身所支配的四大元素的部分的，完全制御魔術式的能力嗎……」

本拉多的眼神一時變得驚愕。

「難道，難道利維拉斯國的神靈、利維伊大人真的顯現在我的面前了嗎……？」

本拉多淚流滿面。

他本以為自己耗費一生的研究毫無意義，打算自滅。但是，直到剛才還不信任的神，淳淳教導自己，輕易地向他演示出他所理想的、窮究魔術的一角。

『本拉多喲，成為四大神官之一，為了余、為這個混沌的利維拉斯國帶來秩序吧』

「是，是的！我，我本拉多……將改變自己至今為止的猜疑，領受這個重大的使命！」

在那一天，本拉多成為了新利維伊派的大神官，依靠新利維伊派的資金開始了比以往更大規模的魔術研究。

他以大型魔獸的制御和強化為中心開展研究，用作新利維伊派的重要戰力，並且染指了禁忌的精靈創造。

他接受了睿智的利維伊的助言，在這高齡裡以前所未有的效率在研究中取得進展。
終於可以擺脫停滯和低迷的日常，本拉多起初是這樣以為。
之前他感覺自己在水池裡浸溺，但是現在他卻覺得自己沉到海裡，他隱隱約約開始覺察到，起初以為全知的利維伊所能以最小單位的魔術式自在操縱的，僅僅只限於水之魔術。

它向本拉多展示的魔法陣的分離和變形，似乎並非是在當場編織，而更像是它預先花費時間編制的東西。
那時，本拉多明白到自己不會再有轉機了。

他餘下的壽命不足二十年。對於神格的利維伊，換個角度看來，它雖然是處於自己的魔術探求之路的延長線上，但絕非絕對的存在。

利維伊想要的只是方便驅使的手下，本拉多的研究也只不過是它用來打倒將要復活的庫多爾、確保自己的統治的道具。

到頭來，本拉多的立場毫無改變。再這樣繼續作為利維伊的手足被驅使，自己就完了。
本拉多明悟到這一切，但他假裝沒有覺察，一味繼續進行研究。
他覺得自己終有一天會成為到達真理的魔術師。
他繼續跟著水神利維伊，因為那是比其他的魔術師更加優越的位置。
在成為黃金之鍊金術師的道路上，最接近終點的就是自己。還有一步也許就能撥雲見日了。

就在本拉多受利維伊之令去攻擊琺基領，以及討伐亞貝爾的時候。

『由於是強行連接，阿茲達哈卡在精靈上恐怕是很不穩定的存在。通過魔力波長引起共振，精靈體單位的連接被完全切斷了』

本拉多最後的王牌，邪精靈龍阿茲・達哈卡。那是鍊金術禁忌的象徵的人工精靈。而看到那個男人，這個人工精靈露出退縮似的表情，被他像驅除害蟲一樣毫不客氣地徹底封殺了。

自己最後的王牌被毫無意義地一擊消滅，但本拉多既不憤怒，也不悲傷。
他的眼前，亞貝爾猶如背後發出光芒。
對魔術有很深的認識的本拉多，明白了亞貝爾是規格外的存在。

同時，他開始覺得利維伊的魔術歸根到底也只是和他自己一個水平，而映照在他眼中的亞貝爾則是超越神的存在。
