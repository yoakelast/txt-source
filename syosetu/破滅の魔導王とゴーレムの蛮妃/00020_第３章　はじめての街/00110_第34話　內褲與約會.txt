「早上好……喂，你在幹什麼，哥雷？」

早上醒來後，發現哥雷把臉埋在我脫下的內褲裡。

昨天睡覺前把脫下的衣服扔得亂七八糟的。想著今天早上再洗衣服。

她把自己的臉埋在我內褲裡紋絲不動。

總覺得看上去有點恍惚。我想，一定是心理作用而已。

這可能是用氣味來檢查衣服的污漬情況吧……我猜。是否需要洗滌，現在應該已經判別出來了吧。

在盆地的隱藏之家的時候，她在洗衣服之前也經常會這麼做。

我雖然知道你沒有惡意，但對於我來說有些羞恥，所以希望你不要這麼做。住手吧……。

不管我說她幾次，她都還是會偷偷地聞味道。

那麼，像這樣，說明哥雷是有嗅覺的。

話說回來，關於這個嗅覺，也並不是那麼發達。根據我所看到的，恐怕只是普通人的程度吧，可能還要比那低下。想要判斷洗衣物的味道，應該是需要花費很長時間。

另一方面，我覺得她有耳朵的優勢，就是說聽力很敏銳。

即使是很小的聲音，也能敏銳地做出反應。

但是，她卻完全聽不進人們說的話。幾乎就只聽我說的。不是很懂啊，這傢伙的聽覺……。

哥雷的五感可以說是出類拔萃的，無論怎麼說，肯定是「視力」。

這傢伙的眼睛非常厲害。特別是動體視力完全超越了人智所能達到的水平。這出色的視力和那個迷之雷達並用的話，哥雷就時刻都能正確掌握著周圍的狀況。

根據昨天禿子所說的話，聖堂格雷姆多虧了被稱為「光受容體」的水晶狀瞳孔的器官，所以視力極高。這是指哥雷那綺麗的紅寶石似的眼睛吧。我記得，聖堂的爆ru們也有著與哥雷不同的綠虹色眼睛。如果說哥雷像紅寶石，那她們就是綠寶石吧。

只是，這個光受容體配在格雷姆身上，其實是十分稀有的。

通常，格雷姆的各種感覺是由被稱為「索敵紋」的額頭上的傳感器所負責的。

沒錯。就是那個額頭上的不可思議的紋樣。

用額頭上的傳感器，能感受到光與聲音，根據格雷姆的不同種類，似乎還能感知到氣味。雖是這麼說，眼睛就並沒有這麼出色了。

若格雷姆的頭部遭到攻擊而停止機能的話，大概就是因為這些傳感器集中在頭部的緣故吧。

話說，哥雷的索敵紋平時基本上都會藏在劉海裡。

這傢伙好像覺得被我看到索敵紋的話會很害羞。如果露出額頭的話，就會慌慌張張地用劉海遮住。

而且，哥雷在聽我講話時會拚命地搖晃耳朵，問我內褲的味道時也會拚命地按在鼻子處。

……哥雷額頭的傳感器功能真的沒問題嗎。有點擔心。

那樣逐漸地研究關於哥雷的傳感器的我，突然回過神來，看了看哥雷的情況。

哥雷還把臉埋在我的內褲裡。

好像有點久了……。

「吶，哥雷，檢查洗衣物的味道，到這種程度就夠了吧？」

我一邊向她搭話，一邊窺視她的臉，我嚇了一跳。

――把臉埋在我內褲的哥雷的眼睛，變成了渾濁的粉紅色

果然是故障啊。

我慌忙地從哥雷那拿回內褲。

騙人的吧？難道，我的內褲有那麼重的味道嗎。

重到讓哥雷出現故障……？

我重新看了看手裡拿著的內褲。

沒有什麼特別奇怪的，就只是拳擊內褲。

這東西是和睡衣一起從原來的世界帶來的我的內褲。和睡衣不同，我覺得這個不土氣。大概。

雖然自己也聞了聞確認下味道，但也並沒有很臭。而且這條內褲也只是昨天穿了一天而已。

不過，好像是有說過自己的體臭自己不知道。更何況，哥雷現在已經出現了故障，這個純粹的事實是不可否認的。

是嗎，果然我的內褲的味道很重嗎……。

我深受打擊。

------

「哈？想買新的內褲？」

禿子用無語的表情看著我。

我含淚點頭。

「嗯……。由於種種原因，我沒帶多少內褲。那個，味道什麼的……。有點在意各種地方……。」

實際上，我的內褲幾乎都被古代地龍給毀滅了，現在就只剩下兩件。那時候穿著的一件和睡衣一起作為貴重物品放在包裡，所以平安無事，還有那個拳擊內褲。

姑且先放下今早發生的事，不提前買新的話，今後肯定會很困擾。如果就這樣用兩條換著穿，並下了一整天雨洗不了衣服的話，就會進退兩難。

「如果是這樣的話，現在去街上的服裝店怎麼樣？我想店應該也開門了。話說，你有說過想看下書吧。也可以順便去書店看看。」

「就這麼做吧……。」

服裝店和書店。剛好有機會。

除了內褲以外，還有很多各種想買的東西。

順帶一提，這個世界的男士內褲有幾種類型，基本上多數都是像緊身褲一樣的形狀。應該不是兜襠佈。這裡沒有橡膠，腰的部分是用繩子輕輕綁住。也沒有特別粗糙，穿起來感覺也不差。

在這裡我突然有了個疑問。

「吶，對了，內褲的價格大概要多少？」

「你連內褲的價格都不知道嗎……。真的是令人無語的在溫室長大的啊。嘛，男士的話，應該50枚銅幣就能買到還不錯的品質。」

說起50枚銅幣，額……。大概是有多少價值呢？

我記得，在店裡吃飯大概要10枚左右的程度吧。我記得不太清了。也就是說，內褲的價值大概相當於餐館的5頓飯嗎？比起原來的世界，衣服的價格稍微高了點。

從這個世界表面的文明水平來看，原以為衣料會很昂貴，但實際上卻並非如此。

「原來如此。總之，這種程度的話，我毫無疑問也能買到。」

我馬上就握緊錢包打算出門。

「喂，內馬凱。」

不知為何被禿子叫住了。

禿子看著我的擔心的眼神，完全是，看著第一次出門跑腿的幼兒園兒童的眼神一樣。（總覺得店主對男主萌生了什麼不得了的感情）

「因為我今天要為了去吉貝爾的商會做各種準備，所以不能跟著你去買東西……。你一個人真的沒問題嗎？」

「啊！？別太小看我了？區區內褲我還是能自己買的！」

我氣呼呼地想打開門的時候，蹲在地板上的哥雷搖搖晃晃地站了起來。

喔呀，你也打算跟著來買東西嗎。

她的瞳孔渾濁成粉紅色，還出現了故障，為了謹慎一點，我想還是讓她在屋裡安靜地看家。

話說，哥雷那傢伙，現在還是處於微妙的半頹廢狀態，真的沒問題嗎……？

------

帶著笑容的中年男性，指著小巷前面的一家舊店。

「那裡是這個鎮上書籍最齊全的書店。」

「非常感謝。讓你特意地給我帶路，真是不好意思。」

我有點過意不去，所以也向男性道謝。

我正當不知書店在哪而困擾的時候，這個人給我帶路。

「不不，哪裡的話。如果能找到不錯的書就好了。一定要好好享受購物的樂趣哦，格雷姆使先生。」

男性微笑著向我揮手後離開了。

真是個好人……。

我跟哥雷與帶路的男性分別後，面向書店，並排走過去。

今天的搭檔，一直在我的後半步的位置走著。這作為平時走路的位置來說，可以算是最前面的位置。從感覺上來看，幾乎是處於橫向並排的狀態。

作為非常喜歡斜後方的哥雷，這是比較新奇的位置所在。

我們友好地並排走著，兩人的手裡都提著裝有大量內褲和衣服的購物袋。

沒錯。先前，即使我屬於幼兒園兒童水平以下，也出色地完成購物。

話說，內褲非常便宜。禿子說大概需要花費50枚銅幣左右，但實際上是20枚銅幣，不知為何服裝店的大叔笑瞇瞇地贈送了我襪子什麼的。

順帶一提，剛才帶我來書店的那位男性，其實就是那個服裝店的大叔。

這也太好人了吧……。

哥雷心情很好地拿著裝有大量內褲的購物袋

兩人一起買東西並排走在街上的時候，她的耳朵一直微微搖晃著。

這傢伙也許是很喜歡購物。

在服裝店買我的褲子時，也很認真地在挑選。

雖然我覺得每條都是一樣的褲子，但這傢伙好像有自己的講究。

話說，穿的人是我啊。哥雷拘泥於這個幹嘛呢……。

那麼，就進書店去看看吧。

打開木質的門，一走進店內，我就聞到了油墨的香氣。這是我非常喜歡的書店的味道。

店鋪的外觀雖然陳舊，但店內整理得很周到。

作為店鋪的大小，比禿子的魔道具店還小得多。倒不如說，禿子商店只是無謂地寬敞而已。

書架上擺著各種各樣的書。

從圖畫書到專業書那樣的東西，好像各種都有備齊。

正如服裝店的店主所說，確實感覺是齊全了很多書類。雖然與原來世界的書店相比還差得很遠。

噢，學習用的書也有。

真遺憾，被古代地龍毀滅的我愛用的『魔術入門』系列，好像這家店裡沒有。

如果有的話，還想重新買一本……。

手裡拿著一本放在附近的似乎是魔術學習用的書籍。

喔呀，書被紙帶封著。看不到裡面的內容。

看來，書架上的書幾乎都封著同樣的紙帶。

原來如此，這裡沒有站著閱讀的文化嗎。

我有著會使魔術專業書的翻譯內容猩猩化的那個現象的問題。不確定裡面的內容就購買學習用書的話，風險會有點高。

還是買別的書吧。

我輕輕地把學習用書放回書架裡。

往店裡看去，好像就只有老婆婆在看店。

嗯，就只有老婆婆一人。

原來如此。我與書店裡可愛的孫女相遇並戀愛的故事等等的，好像不曾開始。

嗯，我是知道的……。

我並沒有在悲傷什麼的。今天也只是來買書而已。

總之，先選一本關於格雷姆的好書吧。

此外，還可以再買一本其他的書。為了享受個人興趣的書。

我可是個喜歡讀書的男人。

煩惱後的結果，我決定買這兩本書。

其名字直截了當，『格雷姆圖鑒』和『能吃的野草』。

『格雷姆圖鑒』，正如其名，就是格雷姆的圖鑒。根據店裡老婆婆所說，附帶插圖會看上去比較容易理解，所以就選了這本書。

另一本『能吃的野草』，連我自己都覺得做出了不錯的選擇。這本似乎有助於野外生存，我也單純地對異世界植物感興趣。也就是說，這是趣味和實益兼併的書。

野草的書是2枚銀幣，格雷姆的書是一枚銀幣和10枚銅幣的價格。

這個世界的書的價格好像相當的高。

不過，事實上這也已經打了很大的折扣。

老婆婆兩本都給我減價了很多。最初的賣價竟然是，野草的8枚銀幣，格雷姆的6枚銀幣。共計14枚銀幣的商品，現在只需3枚銀幣。真是驚人的折扣率。

蒂巴拉鎮上的人們，不知為何對我和哥雷非常友好。

服裝店的店主也是這樣，大家都笑嘻嘻地搶著來指路給我，只是站在商品面前猶豫要不要買，價格就會不斷下降。

攤子上的大叔，也免費給我們小吃。

剛才又收到了烤點心，我打算帶回去當做給蒂侖醬的禮物。

但是，來到鎮上的第一天，我想應該沒有這樣的事……。

特別是書店的老婆婆，含淚一直握著我的手。到了這一步，已經是完全超越了理解的迷之現象。我甚至覺得有可能是謎一樣的老婆婆限定的桃花期的到來。

話是這麼說，即使得到了很好的優惠，買東西的量本來就很多。如果合計衣服的費用的話，我想，恐怕花費了總計銀幣20枚或21枚的量。

一枚銀幣就能住一晚不錯的旅館。雖然不太清楚，但今天一天應該花了20萬日元左右吧。（換成人民幣大概是一千三左右）

稍微有點擔心了，試著數了數錢包裡的錢幣。

金幣5枚，銀幣14枚。銅幣……嘩啦嘩啦地增加了一大堆。恐怕是買東西的時候，都用銀幣來支付了。

我雖然還拿著猴子的魔導核，不過現在沒有現金收入的途徑，不能再浪費更多的錢了……。

嗯，明天開始努力節約吧。

我依舊帶著薄弱的危機感，令人可怕的隨意地管理金錢賬目，然後我們踏上了回家的路。

在旁邊拿著裝有內褲的購物袋的哥雷，看上去很幸福的樣子。