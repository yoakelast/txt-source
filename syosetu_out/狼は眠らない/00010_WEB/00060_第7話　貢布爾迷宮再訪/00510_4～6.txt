４

剛進入那房間，達古隊長便敬了個誇張的禮。

「將冒険者雷肯殿帶來了！」
「亨吉特大人允許那位人士入室。」
「是！雷肯殿，入室吧。」

雷肯站在算不上完全進了房間的位置上。
畢竟在這不怎麼大的房間裡，有個一身裝備豪華過度的騎士，兩個並非那種打扮的騎士，和兩個穿著誇大衣裝的看似文官的男人，佔據了大部分的面積。
為了給雷肯入室的空間，達古隊長得跟房間角落的士兵肩並著肩才行。

「向亨吉特大人敬禮！」

騎士之中的一人叫喚著，但雷肯保持沉默站著。

「算了算了。突然和高位的騎士面對面，渾然忘我了吧。原諒他吧。喂，叫雷肯的。」

雷肯靜靜地俯視著亨吉特。
亨吉特的身高，是這房間裡最矮的。跟雷肯之間的差距，說得上是大人小孩之差。

「聽說，讓下層的迷宮品納入了我等家中。值得稱讚。今後也多加勉勵。若掉落了恩寵品，必定要在此城鎮拍賣。知道嗎。知道的話就可以走了。」

雷肯沒有行禮直接離開了房間。後面能傳來亨吉特的說話聲。

「隊長。兄長還沒從迷宮出來嗎。」
「是的，還未出來。」
「為了領民，親自深入下層以探求恩寵品，這正是所謂武家的典範。為防止兄長有所萬一，傾盡全力吧。」
「是！」

雷肯想著，沒有方法能保護探索迷宮的人的安全吧，並離開了警備隊勤務所。


５

從第一到第十階層，都沒有戰鬥直接下去了。

但是，如果放置纏上來的魔獸的話，魔獸會全擠在下降處前，所以砍下了追上的魔獸的腳，讓其無法行動。
沒有像上次在階梯上奔跑。不過，因為雷肯每步都走兩段階梯，還是比其他的冒険者快。
到第十階層都感受不到任何手感，不會想再來一次了。
在第十一階層前的階梯休息一會，拿出了食物。

取出了銀色戒指戴在左手中指上。
把上次達古隊長給的手冊拿出來看了看。
是這迷宮各階層會出現的魔獸的一覽表，也對各魔獸做了簡單的說明。
開頭說明了迷宮的特色。

「貢布爾迷宮，雖然是只有三十階層的中規模迷宮，但出現的魔獸有各式各樣，除水妖族和龍種外的所有系統的魔獸都有被確認到。在這個迷宮，任何人都能選擇跟自己的戰鬥方式合得來魔獸來加以狩獵。從第一階層到第十階層為洞窟型，第十一階層到第二十階層為無限支柱型，第二十一階層到第三十階層為連續石室型。」

上層，也就是第一到第十階層，適合初心者單人或少人數來戰鬥。中層，也就是第十一到第二十階層，適合中級者組隊戰鬥。
但是到了下層，也就是第二十一到第三十階層，以單獨戰鬥來說魔獸太強，但以大人數戰鬥的話場所又太狹小。因此，下層在冒険者之中沒有人氣。

而這個下層，正是雷肯要前往的場所。


６

下到了第十一階層。

從這裡到第二十階層，基本上也只是直接通過，但通路上有魔獸的場合就會戰鬥。
到這裡就輕鬆多了。畢竟一直到第十階層，通路都既昏暗又狹窄，冒険者也多，沒辦法自由選路走。

第十一階層的魔獸是，鼻曲。
鼻曲是豬鬼族的第三階位魔獸，是雷肯在這個世界首次遭遇的魔獸，也是與露比安菲露邂逅的契機。
雷肯直接快步抵達了階梯，沒有在這階層戰鬥。

第十二階層的魔獸是，長腕猿。不過，比同種族的帕雷多小很多，跟傑利可比更是又小了一圈。
在這裡也沒戰鬥。
第十三階層的魔獸是，蛇凶族第四階位魔獸的平白蛇。
見到這魔獸讓雷肯感到不愉快。身體會回想起被迫喝下了平白蛇的毒的痛苦回憶。
雷肯舉起右手，握住手掌只伸出食指，在體內一點一滴集中著魔力，並詠唱了咒文。

「〈炎槍〉！」

指尖前產生的小小的火焰，迅速膨脹成細長的大炎塊，向魔獸飛去。
平白蛇是對熱度敏感的魔獸。是察覺到了這個魔法並等待著機會吧。向右方快速閃過，接著襲擊雷肯。
雷肯用右手從〈收納〉取出了劍，將跳過來的平白蛇一刀兩斷。

（發動得太慢了）

不管多有威力，從詠唱咒文到發動要這麼久的話，在實戰就毫無用處。
雷肯用右手食指指向迷宮中數不清的，宛如枯木的柱子中的一柱。

「〈炎槍〉！」

這次，魔法比剛剛發動得要快了一半。
果然，在腦中迅速確實地描繪的話，魔法的發動就會變快。
接著是讓垂著的右腕，突然伸向別的柱子。

「〈炎槍〉！」

什麼都沒發生。
雷肯領悟到了失敗的原因。在詠唱咒文之前，懈怠了魔力的收束。魔法自然是不會發動。

提煉魔力。
指向目標。
詠唱咒文。

這個順序，要確實無誤，而且得快速地實行。

（不論是劍的招式還是魔法）（到頭來都是一樣的阿）

從學會一個招式，到能夠在實戰中使用，中間需要累積足夠的經驗。更進一步而言，在連續的戰鬥中，要在最為必要的瞬間，以有效的方式使用招式，更有必要積累工夫與實踐。

就算是威力弱小，不吸引人的招式，熟練的話也能發揮意想不到的效果。
更不用說身為劍士的雷肯，得到了〈炎槍〉這樣的強力遠距離攻擊，這其中有著重大的意義。
今後也得充分鑽研才行。