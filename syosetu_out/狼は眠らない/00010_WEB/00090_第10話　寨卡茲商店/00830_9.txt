艾達抱怨說，想在柯古陸斯城逗留幾天。

剛入手了大筆錢，又是在沒見過的大城鎮裡。想到處逛街購物的女人心，也不是不懂。
但是，雷肯想盡快解決麻煩事。

敵人雖然應該不會在自己居住的都市鬧事，但所謂暗殺者，是會出其不意的。在這種街上被襲擊的話，可不好迎擊。

「那就分別行動吧。我先走一步，離開這城鎮。」
「就那樣吧。我覺得不錯。可以吧，妮姫小姐。」
「不太行喔。」
「誒誒？」
「小艾達，是想在這城鎮買東西吧？」
「還有料理喔。想吃點好吃的。」
「貢布爾的宿屋的料理很美味對吧。」
「最棒了！」
「購物跟料理，到貢布爾去如何呢。」
「可以啊。但是那個宿屋好像很貴。」
「不夠的話就幫你付吧。」
「真的嗎？不過，嗯─，經過貢布爾鎮的時候，沒看到什麼很棒的服飾店。」
「畢竟那裡不是女性為了漂亮打扮而去的城鎮呢。但要這麼說的話，這城鎮的衣服也不太能期待喔。畢竟是鄉下呢。」
「誒？是這樣嗎？」
「我想沃卡反而有比較多漂亮的衣服吧。而且呢，小艾達，有逛過沃卡城的高級服飾店嗎。」
「沒有。」
「那麼，錢應該在沃卡用才好。」
「知道了。」

就這樣，時間明明快到傍晚，雷肯等人仍離開了柯古陸斯城。出城時收了相當高額的稅金。接著便快步移動到山裡去。

在稍微空曠的場所，雷肯停了下來。

「好。我在這裡野營。你們先走。」
「誒誒誒？為什麼阿。我們也在這裡野營吧。」
「不行喔，小艾達。」
「為什麼不行？」
「我們要先走一步。」
「為什麼？」
「為了幫你做魔法的練習喔。」
「走吧。」
「那，首先是〈燈光〉。一個在二十步前，一個在兩步前點亮，配合著走路速度去移動。試試看吧。」
「知道了。」

艾達練習著燈光，開心地走掉了。
妮姫說了句話就離開了。

「加油啊。」
「啊啊。」

雷肯迅速地撿拾收集枯木頭，並用魔法點火。

坐在倒下的木頭上，對著篝火伸手取暖時，〈生命感知〉上表示的兩個紅點，向雷肯靠近而來。
雷肯的左手中指上的銀色戒指，淡淡地發著光。最近一直都戴著。
雷肯完全沒聽到可疑的聲響。到了這距離都沒被雷肯察覺到氣息，相當異常。兩人都是很厲害的暗殺者吧。擁有什麼能消除氣息的技能也說不定。

對方用上許多時間來縮短距離。靠近的時機很不錯。
然後，接近到二十步的距離時，穩穩地停在那裡。另一個紅點繞了一大圈，移動到反面去。

「馬拉奇斯，是個好父親嗎？」

雷肯的質問陷入了漆黑的森林中。雷肯正想著是不是沒打算回應時，多波魯的臉從黑暗中浮現而出。

「從沒把那種男人當作是父親。」
「但卻要復仇嗎。」
「確實說得上是復仇。但不是為了馬拉奇斯的復仇。寨卡茲商店花了長久的時間、工夫與金錢，在沃卡建立了地盤。而你破壊了那地盤。對於此事，不做個了結可不行。」
「我對寨卡茲商店無恩無怨。也沒有破壊過地盤。」
「沃卡分店長因為你而死了。」
「那個人現在不是受到逮捕，正在被審問嗎？」
「然後，八位貴重的戰力，因為你而喪失了。」
「八位？」
「雖然期間很短，但被任命為臨時分店長的我，不能默默地放你回去。」
「這是札克的指示嗎？」
「不是。是我的判斷。」
「〈炎槍〉！」

確認到另一個敵人開始從正後方筆直接近，雷肯用垂著的右手掌，向背後放出了〈炎槍〉。

然後將右手收回並拔出劍，襲向前方的多波魯。
多波魯將身體翻了一圈，消失於樹木之中。雷肯打算衝入樹木中追上去，但有什麼東西朝雷肯的頭部飛了過來。
反射性地閃躲過，那物體又改變了軌跡襲來。雷肯以動物般的反射神經轉動了頭，但額頭仍被兇器給擦到。

雷肯往後大跳，但這次從後方飛來了什麼。
背後的襲擊者躲開了雷肯的〈炎槍〉。
雷肯翻轉身體，閃過從後方飛來的小刀。這是沒有以〈立體知覺〉捕捉對方的行動，就不可能做到的動作。

前方飛來兇器。
在這時間點，已經大致以〈立體知覺〉得知了兇器的真面目。

多波魯操縱著細繩般的東西。繩頭部分連接著尖銳的錘頭。
剛才，是讓繩子接觸樹木，以彎曲改變軌道。

雷肯毅然縮短了與前方敵人的距離。後方雖然飛來小刀，但不去閃躲，繼續向前。
這行動似乎稍微出乎了多波魯的意料之外，投擲兇器的時機晚了些。這遲疑對雷肯這等人物來說，是致命的遲疑。雷肯後背雖然被小刀直擊，但小刀沒能穿透〈貴王熊的外套〉。
雷肯閃過從眼前逼迫而來的兇器，砍了繩子。

然而，切不斷。繩子非常的堅固。
但是多波魯在一瞬間失去了兇器的掌控。雷肯在這瞬間逼近多波魯，揮下白刃。

突然，全身麻痺。

（怎麼可能！）

雷肯左手戴著的銀色戒指，賦予了〈狀態異常耐性〉、〈毒耐性〉以及〈詛咒耐性〉。明明戴著這戒指，卻產生麻痺，這究竟是。
是多波魯在兇器上塗的毒的效果超越了戒指的耐性嗎。還是說，是毒以外的什麼嗎。

多波魯沒可能無視雷肯身上發生的硬直。拔出了短劍朝臉上突刺而去。避開外套瞄準露出的臉，漂亮的判斷。
移開受到麻痺而不能自由行動的臉，是雷肯這時間點能做到的最大的防禦。用顴骨承受短劍，來偏移刀鋒的攻擊。

這嘗試成功了一半。
多波魯刺入的短劍，沒能深深插入雷肯的臉。

相對的，深深地挖開了臉頰，削過顴骨。
這短劍肯定也塗滿了毒。銀色戒指無效的話，雷肯很快就會連動也動不了吧。
多波魯此時正衝入了雷肯懷中。

「〈炎槍〉！」

雷肯從左手掌放出了〈炎槍〉。從極近距離放出的魔法之槍，將刺向雷肯肚子的短劍，連同多波魯的右手一起擊飛，在多波魯胸口開了大洞，飛入森林之中。
背後的敵人馬上從後方逼迫而來。手上拿著細長的像是劍的東西。

「喔喔喔喔喔喔！」

雷肯轉向後方，同時大幅度揮出了劍。雷肯的體型很大，手腕很長。因此劍的攻擊半徑也很大。突進而來的對手雖然低下身子想閃過劍，但雷肯讓自己倒下，修正了劍的軌道。
後方的敵人被從胸口砍成了兩半，即死。

是基多。
是過來時做馬車車夫的老人。

雖然有推測出擁有暗殺技術，但沒想到會這麼有實力。
全身宛如燒起來似地炎熱。
雖然想取出藥水，但身體無法自由行動。

絶不能就這樣失去意識。

有誰接近了。

是誰。

是妮姫和艾達。
雷肯中止了意識的保持。